<?php

namespace App\Events;

use App\Models\Trip;
use Illuminate\Queue\SerializesModels;

class TripUpdated extends Event
{
    use SerializesModels;

    public $trip;

    public $changes;

    /**
     * Create a new event instance.
     *
     * @param \App\Models\Trip $trip
     * @param array $changes
     *
     * @return void
     */
    public function __construct(Trip $trip, array $changes)
    {
        $this->trip = $trip;
        $this->changes = $changes;
    }
}
