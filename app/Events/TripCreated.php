<?php

namespace App\Events;

use App\Models\Trip;
use Illuminate\Queue\SerializesModels;

class TripCreated extends Event
{
    use SerializesModels;

    public $trip;

    /**
     * Create a new event instance.
     *
     * @param \App\Models\Trip $trip
     * @return void
     */
    public function __construct(Trip $trip)
    {
        $this->Trip = $trip;
    }
}
