<?php

namespace App\Models;

use App\Traits\ModelValidatable;
use App\Traits\QueryFilterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Trip extends Model
{
    use Authorizable, QueryFilterable, ModelValidatable, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'title', 'where', 'start_date', 'end_date', 'type_trip', 'description',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
    ];

    /**
     * Validation rules for the model.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            '*' => [
                'user_id' => 'required',
                'title' => 'required',
                'where' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'type_trip' => 'required',
                'description' => 'required',
            ],
            // 'CREATE' => [
            //     'start_date' => 'required|',
            //     'end_date' => 'required|',
            // ],
            // 'UPDATE' => [
            //     'start_date' => 'required|',
            //     'end_date' => 'required|',
            // ],
        ];
    }
}
