<?php

namespace App;

use App\Events\UserCreated;
use App\Events\UserUpdated;
use App\Events\TripCreated;
use App\Events\TripUpdated;
use App\Models\User;
use App\Models\Trip;
use App\Transformers\UserTransformer;
use App\Transformers\TripTransformer;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class Accounts
{
    /**
     * Get list of paginated users.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function getUsers(Request $request): array
    {
        $users = User::filter($request)->paginate($request->get('per_page', 20));

        return fractal($users, new UserTransformer())->toArray();
    }

    /**
     * Get a user by ID.
     *
     * @param  int  $id
     * @return array
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function getUserById(int $id): array
    {
        $user = User::findOrFail($id);

        return fractal($user, new UserTransformer())->toArray();
    }

    /**
     * Store a new user.
     *
     * @param  array  $attrs
     * @return array
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeUser(array $attrs): array
    {
        $user = new User($attrs);

        if (!$user->isValidFor('CREATE')) {
            throw new ValidationException($user->validator());
        }

        $user->save();

        event(new UserCreated($user));

        return fractal($user, new UserTransformer())->toArray();
    }

    /**
     * Update a user by ID.
     *
     * @param  int  $id
     * @param  array  $attrs
     * @return array
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateUserById(int $id, array $attrs): array
    {
        $user = User::findOrFail($id);
        $user->fill($attrs);

        if (!$user->isValidFor('UPDATE')) {
            throw new ValidationException($user->validator());
        }

        $changes = $user->getDirty();
        $user->save();

        event(new UserUpdated($user, $changes));

        return fractal($user, new UserTransformer())->toArray();
    }

    /**
     * Delete a user by ID.
     *
     * @param  int  $id
     * @return bool
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function deleteUserById(int $id): bool
    {
        $user = User::findOrFail($id);

        return (bool) $user->delete();
    }

    /**
     * Store a new trip.
     *
     * @param  array  $attrs
     * @return array
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeTrip(array $attrs): array
    {
        $trip = new Trip($attrs);

        if (!$trip->isValidFor('CREATE')) {
            throw new ValidationException($trip->validator());
        }

        $trip->save();

        event(new TripCreated($trip));

        return fractal($trip, new TripTransformer())->toArray();
    }

    /**
     * Get a user trip by token.
     *
     * @param  int  $id
     * @return array
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function getTripByUserID(int $user_id): array
    {
        $trip = Trip::where('user_id', $user_id)->first();
        
        return fractal($trip, new TripTransformer())->toArray();
    }

    /**
     * Update a Trip by ID.
     *
     * @param  int  $id
     * @param  array  $attrs
     * @return array
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateTripUserById(array $attrs): array
    {
        $id = $attrs['id'];

        $trip = Trip::findOrFail($id);
        $trip->fill($attrs);

        if (!$trip->isValidFor('UPDATE')) {
            throw new ValidationException($trip->validator());
        }

        $changes = $trip->getDirty();
        
        $trip->save();

        event(new TripUpdated($trip, $changes));

        return fractal($trip, new TripTransformer())->toArray();
    }

    /**
     * Delete a trip by ID.
     *
     * @param  int  $id
     * @return bool
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function deleteTripById(array $attrs): bool
    {
        $id = $attrs['id'];

        $trip = Trip::findOrFail($id);

        return (bool) $trip->delete();
    }
}
