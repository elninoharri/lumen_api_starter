<?php

namespace App\Transformers;

use App\Models\Trip;
use League\Fractal\TransformerAbstract;

class TripTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param  \App\Models\Trip  $Trip
     * @return array
     */
    public function transform(Trip $trip): array
    {
        return [
            'user_id' => (int) $trip->user_id,
            'title' => (string) $trip->title,
            'where' => (string) $trip->where,
            'start_date' => (string) $trip->start_date,
            'end_date' => (string) $trip->end_date,
            'type_trip' => (string) $trip->type_trip,
            'description' => (string) $trip->description,
        ];
    }
}
